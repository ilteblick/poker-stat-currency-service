require('dotenv').config();

const pgp = require('pg-promise')({});

const log4js = require('log4js');

log4js.configure({
    appenders: { DB: { type: 'file', filename: 'log.log' } },
    categories: { default: { appenders: ['DB'], level: 'trace' } },
});

const end_point_cn = {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    database: process.env.DB_END_POINT_DB_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
};


const end_point_db = pgp(end_point_cn);

async function createHistoryCurrencies() {
    for (let i = 2005; i < 2018; i += 1) {
        for (let j = 1; j <= 12; j += 1) {
            const monthToWrite = j < 10 ? `0${j}` : j;
            await end_point_db.any('INSERT INTO currencies (currency_id, value, hm_format_date) VALUES ($1, $2, $3);', [0, 1, `${i}${monthToWrite}`]);
            await end_point_db.any('INSERT INTO currencies (currency_id, value, hm_format_date) VALUES ($1, $2, $3);', [1, 1.2, `${i}${monthToWrite}`]);
        }
    }
    return Promise.resolve();
}

end_point_db.any(`create table if not exists currencies(
    id serial,
    currency_id integer,
    value double precision,
    hm_format_date integer
)`)
    .then(() => createHistoryCurrencies());
